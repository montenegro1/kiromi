import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'promociones',
      url: '/promociones',
      icon: 'cash'
    },
    {
      title: 'menu',
      url: '/menu',
      icon: 'restaurant'
    },
    {
      title: 'ubicanos',
      url: '/ubicanos',
      icon: 'compass'
    },
    {
      title: 'facturacion',
      url: '/facturacion',
      icon: 'calculator'
    },
    {
      title: 'reservacion',
      url: '/reservacion',
      icon: 'calculator'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
