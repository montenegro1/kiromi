import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
   
  

  
  { path: 'promociones',
   loadChildren: './promociones/promociones.module#PromocionesPageModule' },

  { path: 'menu',
   loadChildren: './menu/menu.module#MenuPageModule' },
  { path: 'ubicanos', loadChildren: './ubicanos/ubicanos.module#UbicanosPageModule' },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
  { path: 'facturacion', loadChildren: './facturacion/facturacion.module#FacturacionPageModule' },
  { path: 'reservacion', loadChildren: './reservacion/reservacion.module#ReservacionPageModule' }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
